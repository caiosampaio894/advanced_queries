create table if not exists redes_sociais(
	id bigserial primary key,
	nome varchar(150) not null unique
);


create table if not exists enderecos(
	id bigserial primary key,
	rua varchar not null,
	pais varchar(100) not null,
	cidade varchar(100)not null
);

create table if not exists usuarios(
	id bigserial primary key,
	nome varchar(100),
	email varchar not null unique,
	senha varchar not null,
	endereco_id integer not null,
	foreign key (endereco_id)  references enderecos(id)
);


create table if not exists usuario_redes_sociais(
	constraint id primary key (usuario_id, rede_social_id),
	usuario_id integer not null,
	rede_social_id integer not null, 
	foreign key (usuario_id)  references usuarios(id),
	foreign key (rede_social_id)  references redes_sociais(id)
);

insert into enderecos 
	(rua, pais, cidade)
values
	('Avenida Higienópolis', 'Brasil', 'Londrina'),
	('Avenida Paulista', 'Brasil', 'São Paulo'),
	('Rua Marcelino Champagnat', 'Brasil', 'Curitiba');

insert into usuarios 
	(nome, email, senha, endereco_id)
values
	('Cauan', 'cauan@exemple.com', '1234', (select id from enderecos where rua like '%Paulista' and cidade like 'São Paulo')),
	('Chrystian', 'chrystian@exemple.com', '4321', (select id from enderecos where rua like '%Marcelino%' and cidade like 'Curitiba')),
	('Matheus', 'matheus@exemple.com', '3214', (select id from enderecos where rua like '%Higienópolis' and cidade like 'Londrina'));

insert into redes_sociais 
	(nome)
values 
	('Youtube'),
	('Twitter'),
	('Instagram'),
	('Facebook'),
	('TikTok');

insert into usuario_redes_sociais
	(usuario_id, rede_social_id)
values
	((select id from usuarios where nome like 'Cauan'),(select id from redes_sociais where nome like 'Youtube')),
	((select id from usuarios where nome like 'Chrystian'), (select id from redes_sociais where nome like 'Youtube')),
	((select id from usuarios where nome like 'Matheus'), (select id from redes_sociais where nome like 'Youtube')),
	((select id from usuarios where nome like 'Chrystian'), (select id from redes_sociais where nome like 'Twitter')),
	((select id from usuarios where nome like 'Cauan'), (select id from redes_sociais where nome like 'Twitter')),
	((select id from usuarios where nome like 'Matheus'), (select id from redes_sociais where nome like 'Instagram')),
	((select id from usuarios where nome like 'Matheus'), (select id from redes_sociais where nome like 'Facebook')),
	((select id from usuarios where nome like 'Chrystian'), (select id from redes_sociais where nome like 'Instagram')),
	((select id from usuarios where nome like 'Cauan'), (select id from redes_sociais where nome like 'TikTok'));

select * from enderecos;

select * from enderecos inner join usuarios on enderecos.id = usuarios.id;

select rds.id, rds.nome, u.id, u.nome, u.email, u.senha, u.endereco_id
	from usuario_redes_sociais urs 
		join usuarios u on u.id = urs.usuario_id
		join redes_sociais rds on rds.id = urs.rede_social_id;
	
select  rds.id, rds.nome, u.id, u.nome, u.email, u.senha, u.endereco_id, e.id, e.rua, e.pais
	from usuario_redes_sociais urs 
		join usuarios u on u.id = urs.usuario_id
		join redes_sociais rds on rds.id = urs.rede_social_id
		join enderecos e on u.endereco_id = e.id;
	
select rds.nome, u.nome, u.email, e.cidade
	from usuario_redes_sociais urs 
		join redes_sociais rds on rds.id = urs.rede_social_id
		join usuarios u on u.id = urs.usuario_id
		join enderecos e on u.endereco_id = e.id;
	
select rds.nome, u.nome, u.email, e.cidade
	from usuario_redes_sociais urs 
		join redes_sociais rds on rds.id = urs.rede_social_id 
		join usuarios u on u.id = urs.usuario_id
		join enderecos e on u.endereco_id = e.id
			where rds.nome like 'Youtube';
		
select rds.nome, u.nome, u.email, e.cidade
	from usuario_redes_sociais urs 
		join redes_sociais rds on rds.id = urs.rede_social_id 
		join usuarios u on u.id = urs.usuario_id
		join enderecos e on u.endereco_id = e.id
			where rds.nome like 'Instagram';
		
select rds.nome, u.nome, u.email, e.cidade
	from usuario_redes_sociais urs 
		join redes_sociais rds on rds.id = urs.rede_social_id 
		join usuarios u on u.id = urs.usuario_id
		join enderecos e on u.endereco_id = e.id
			where rds.nome like 'Facebook';

select rds.nome, u.nome, u.email, e.cidade
	from usuario_redes_sociais urs 
		join redes_sociais rds on rds.id = urs.rede_social_id 
		join usuarios u on u.id = urs.usuario_id
		join enderecos e on u.endereco_id = e.id
			where rds.nome like 'TikTok';
		
select rds.nome, u.nome, u.email, e.cidade
	from usuario_redes_sociais urs 
		join redes_sociais rds on rds.id = urs.rede_social_id 
		join usuarios u on u.id = urs.usuario_id
		join enderecos e on u.endereco_id = e.id
			where rds.nome like 'Twitter';